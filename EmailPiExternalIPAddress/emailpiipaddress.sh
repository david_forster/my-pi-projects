    #!/bin/sh

    SUBJ="RaspPI_External_IP_Address"
    EMAIL="forster.inc@gmail.com"

    ip1=""
    ip2=""

    read ip1 < /usr/share/adafruit/webide/repositories/my-pi-projects/EmailPiExternalIPAddress/ip.txt
    ip2=$(wget -qO- ifconfig.me/ip)

    if [ "$ip1" = "$ip2" ]
    then
      exit
    else
      echo "$ip2" > ip.txt
      echo "$ip2" | mail -s $SUBJ $EMAIL
      exit
    fi